/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

import java.util.ArrayList;

public class researchGroup {
    private ArrayList<Patient> research_patients = new ArrayList<Patient>();
    private String name;

    researchGroup(String name, ArrayList<Patient> research_patients){
        this.name = name;
        this.research_patients= research_patients;
    }
    
    public ArrayList<Patient> getPatients() {
        return research_patients;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
